import React from 'react'
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import PostTable from './PostTable';
import Comments from './Comments';

function App() {

  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<PostTable />} />
        <Route path="/comments" element={<Comments />} />
      </Routes>
    </Router>
  );
}

export default App;

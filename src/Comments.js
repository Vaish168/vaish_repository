import React, { useState, useEffect } from 'react'
import "./App.css";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useLocation, useNavigate } from 'react-router-dom';

function Comments(props) {
  const location = useLocation();
  let activePage = location.state.activePage;
  let postId = location.state.postId;
  const theme = createTheme();
  const [commentsData, setCommentsData] = useState([]);
  const [errorMsg,setErrorMsg] = useState('');
  const navigate = useNavigate();

  useEffect(() => {

    let url = "https://jsonplaceholder.typicode.com/posts/" + postId + "/comments?_limit=10&_page=1";
    fetch(url)
      .then((data) => data.json())
      .then((data) => {
        setCommentsData(data);
      })
      .catch(error => {
        setErrorMsg('Oops! Something went wrong! Try again later!')
    });
  }, [postId])

  const goBack = () => {
    navigate("/", { state: { activePage: activePage } });
  }
  return (
    <ThemeProvider theme={theme}>
      <div className="App wrapper">
        {errorMsg && <div>{errorMsg}</div>}
        {commentsData.map((ct) =>
          <p>{ct.body}</p>)}
        <button type='button' onClick={goBack}>Back</button>
      </div>
    </ThemeProvider>
  );
}

export default Comments;

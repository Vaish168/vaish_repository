import React, { useState, useEffect } from 'react'
import "./App.css";
import MUIDataTable from "mui-datatables";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate, useLocation } from 'react-router-dom';

function PostTable(props) {

  let activePage = 0;
  const location = useLocation();
  if (location.state) {
    activePage = location.state.activePage;
  }

  const navigate = useNavigate();
  const theme = createTheme();
  const [tableData, setTableData] = useState([]);
  const [errorMsg,setErrorMsg] = useState('');
  let postId;
  let postsArray = [];

  const columns = [{ name: 'userId', label: 'User Id' }, { name: 'title', label: 'Title' },
  { name: 'body', label: 'Body' }, {
    name: 'postId', label: 'Post Id', options: {
      display: false,
    }
  }];

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts?_embed=comments")
      .then((data) => data.json())
      .then((data) => {
        setTableData(data)
      })
      .catch(error => {
          setErrorMsg('Oops! Something went wrong! Try again later!')
      });
  }, [])

  tableData.map((tb) => {
    tb.comments.map((comment) =>
      postId = comment.postId);
    postsArray.push({ userId: tb.userId, title: tb.title, body: tb.body, postId: postId })
  })

  const options = {
    page: activePage,
    onRowClick(row, rowNo, rowNum) {
      let selectedPostId = row[row.length - 1];
      navigate("/comments", { state: { activePage: activePage, postId: selectedPostId } })
    },
    onChangePage: (currentPage) => {
      activePage = currentPage;
    }
  };

  return (
    <ThemeProvider theme={theme}>
        {errorMsg && <div>{errorMsg}</div>}
      <div className="App wrapper">
        <MUIDataTable
          title={""}
          data={postsArray}
          columns={columns}
          options={options}
        />
      </div>
    </ThemeProvider>
  );
}

export default PostTable;

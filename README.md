# About Project

This project uses :
"react": "^17.0.2" - Javascript library
"react-router-dom": "^6.2.1" - Plugin to handle routing logic
"@material-ui/core": "^4.12.3" - Styling package
"@material-ui/icons": "^4.11.2" - Styling package
"@mui/styles": "^5.4.2" - Styling package
"mui-datatables": "^4.0.0" - Datatables component
"Docker Desktop": "4.4.4" - For running the app using Docker container

## Instructions to run the project using Docker

Install Docker Desktop.

In the project directory, you can run:

### `docker build -t assignmentapp:latest .`

This command is to build the docker image

### `docker run --name assignmentcontainer -d -p 3000:3000 assignmentapp:latest`

This command is to create and run the docker container
Open the react application in the browser from the docker desktop app

## Instructions to run the project in local

Assuming node js and react are already installed.

In the project directory, you can run:
### `npm install`

This command is to install all the necessary packages.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.